package sk.seidl.spring5_webflux.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.reactivestreams.Publisher;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.seidl.spring5_webflux.domain.Vendor;
import sk.seidl.spring5_webflux.repositories.VendorRepository;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-19
 */
public class VendorControllerTest {

    private VendorRepository vendorRepository;
    private VendorController vendorController;
    private WebTestClient webTestClient;

    @Before
    public void setUp() throws Exception {
        vendorRepository = Mockito.mock(VendorRepository.class);
        vendorController = new VendorController(vendorRepository);
        webTestClient = WebTestClient.bindToController(vendorController).build();
    }

    @Test
    public void list() {
        BDDMockito.given(vendorRepository.findAll())
                .willReturn(
                        Flux.just(
                                Vendor.builder().firstName("First_Name_1").lastName("Last_Name_1").build(),
                                Vendor.builder().firstName("First_Name_2").lastName("Last_Name_2").build(),
                                Vendor.builder().firstName("First_Name_3").lastName("Last_Name_3").build()
                        )
                );
        webTestClient.get()
                .uri("api/v1/vendors")
                .exchange()
                .expectBodyList(Vendor.class)
                .hasSize(3);
    }

    @Test
    public void getById() {
        BDDMockito.given(vendorRepository.findById(Mockito.anyString()))
                .willReturn(
                        Mono.just(
                                Vendor.builder().firstName("First_Name_1").lastName("Last_Name_1").build()

                        ));
        webTestClient.get()
                .uri("api/v1/vendors/" + Mockito.anyString())
                .exchange()
                .expectBody(Vendor.class);
    }

    @Test
    public void create() {
        BDDMockito.given(vendorRepository.saveAll(Mockito.any(Publisher.class)))
                .willReturn(
                        Flux.just(
                                Vendor.builder().build()
                        )
                );
        Mono<Vendor> catToSaveMono = Mono.just(Vendor.builder().firstName("First_Name_1").lastName("Last_Name_1").build());
        webTestClient.post()
                .uri("api/v1/vendors/")
                .body(catToSaveMono, Vendor.class)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(Void.class);
    }

    @Test
    public void update() {
        BDDMockito.given(vendorRepository.saveAll(Mockito.any(Publisher.class)))
                .willReturn(
                        Flux.just(
                                Vendor.builder().build()
                        )
                );
        Mono<Vendor> catToSaveMono = Mono.just(Vendor.builder().firstName("First_Name_1").lastName("Last_Name_1").build());
        webTestClient.put()
                .uri("api/v1/vendors/some_id")
                .body(catToSaveMono, Vendor.class)
                .exchange()
                .expectStatus()
                .isOk();
    }
}