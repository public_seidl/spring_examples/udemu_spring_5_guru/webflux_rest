package sk.seidl.spring5_webflux.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.reactivestreams.Publisher;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.seidl.spring5_webflux.domain.Category;
import sk.seidl.spring5_webflux.repositories.CategoryRepository;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-19
 */
public class CategoryControllerTest {

    private WebTestClient webTestClient;
    private CategoryRepository categoryRepository;
    private CategoryController categoryController;

    @Before
    public void setUp() throws Exception {
        categoryRepository= Mockito.mock(CategoryRepository.class);
        categoryController = new CategoryController(categoryRepository);
        webTestClient= WebTestClient.bindToController(categoryController).build();
    }

    @Test
    public void list() {
        BDDMockito.given(categoryRepository.findAll())
                .willReturn(
                        Flux.just(
                                Category.builder().description("cat_1").build(),
                                Category.builder().description("cat_2").build(),
                                Category.builder().description("cat_3").build()
                        )
                );
        webTestClient.get()
                .uri("api/v1/categories/")
                .exchange()
                .expectBodyList(Category.class)
                .hasSize(3);

    }

    @Test
    public void getById() {
        BDDMockito.given(categoryRepository.findById(Mockito.anyString()))
                .willReturn(
                        Mono.just(Category.builder().description("Cast_1").build()
                        )
                );
        webTestClient.get()
                .uri("api/v1/categories/"+Mockito.anyString())
                .exchange()
                .expectBody(Category.class) ;

    }

    @Test
    public void create() {
        BDDMockito.given(categoryRepository.saveAll(Mockito.any(Publisher.class)))
                .willReturn(
                        Flux.just(
                                Category.builder().build()
                        )
                );
        Mono<Category> catToSaveMono = Mono.just(Category.builder().description("cat_1").build());
        webTestClient.post()
                .uri("api/v1/categories/")
                .body(catToSaveMono, Category.class)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(Void.class);
    }

    @Test
    public void update() {

        BDDMockito.given(categoryRepository.save(Mockito.any(Category.class)))
                .willReturn(
                        Mono.just(
                                Category.builder().build()
                        )
                );
        Mono<Category> catToUpdateMono = Mono.just(Category.builder().description("cat_1").build());

        webTestClient.put()
                .uri("api/v1/categories/some_id")
                .body(catToUpdateMono, Category.class)
                .exchange()
                .expectStatus()
                .isOk();
    }
}