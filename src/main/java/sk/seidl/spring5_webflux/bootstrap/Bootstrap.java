package sk.seidl.spring5_webflux.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import sk.seidl.spring5_webflux.domain.Category;
import sk.seidl.spring5_webflux.domain.Vendor;
import sk.seidl.spring5_webflux.repositories.CategoryRepository;
import sk.seidl.spring5_webflux.repositories.VendorRepository;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-19
 */

@Component
public class Bootstrap implements CommandLineRunner {

    private CategoryRepository categoryRepository;
    private VendorRepository vendorRepository;

    public Bootstrap(CategoryRepository categoryRepository, VendorRepository vendorRepository) {
        this.categoryRepository = categoryRepository;
        this.vendorRepository = vendorRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        clearDB();
        if(categoryRepository.count().block()==0){
            // load Data
            System.out.println("####  LOADING DATA ON BOOTSTRAP ####");
            categoryRepository.save(Category.builder().description("Fruits").build()).block();
            categoryRepository.save(Category.builder().description("Nuts").build()).block();
            categoryRepository.save(Category.builder().description("Breads").build()).block();
            categoryRepository.save(Category.builder().description("Meats").build()).block();
            categoryRepository.save(Category.builder().description("Eggs").build()).block();

            System.out.println("Loaded Cateories: " + categoryRepository.count().block());
        }

        if(vendorRepository.count().block() ==0){
            vendorRepository.save(Vendor.builder().firstName("Joe").lastName("Buck").build()).block();
            vendorRepository.save(Vendor.builder().firstName("Michael").lastName("Weston").build()).block();
            vendorRepository.save(Vendor.builder().firstName("Jessie").lastName("Waters").build()).block();
            vendorRepository.save(Vendor.builder().firstName("Bill").lastName("Nershi").build()).block();
            vendorRepository.save(Vendor.builder().firstName("Jimmy").lastName("Buffet").build()).block();

            System.out.println("Loaded Vendors: " + vendorRepository.count().block());
        }

    }

    private void clearDB() {
        categoryRepository.deleteAll().block();
        vendorRepository.deleteAll().block();

    }
}
