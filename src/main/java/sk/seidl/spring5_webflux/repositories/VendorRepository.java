package sk.seidl.spring5_webflux.repositories;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import sk.seidl.spring5_webflux.domain.Vendor;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-19
 */
@Repository
public interface VendorRepository extends ReactiveMongoRepository<Vendor,String>{
}
